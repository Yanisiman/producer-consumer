# Autour du Pattern Producer/Consumer

## Description

Le challenge consiste à implémenter une application qui génère des pseudos Pass d'accès (QR code, PDF ou autres) via une interface utilisateur ou API..

**Fonctionnalités :**
- Être capable de recevoir des lots de demandes de Pass à générer 
- Infos d'un Pass : Prénom, Nom, DateNaissance, Status_VIP, DateHeureDeLaDemande, DateHeureDeGénération
- Les attributs Prénom, Nom, Status_VIP, sont determinés aléatoirement.
- Prioriser les VIP dans la génération des Pass
- Suivre l'état d'avancement (user ou api)
- Être en mesure de récupérer un Pass (user ou api) en fonction de ses attributs Nom, Prénom et DateNaissance

**Critères d'évaluations :**
- Lancement de la génération en 1 seul appel/action ✅
- Pass générés ✅
- Pass récupérés ✅
- Priorité respectée ✅
- Architecture du code ✅
- Scalabilité ✅
- Facilité d'usage ✅

**Contraintes** : 
- Langage libre,
- Implémentation libre,
- Architecture libre,
- Plateforme libre.

**Livrable :** dépôt git avec doc de mise en œuvre
